﻿Public Class Form2
    Private Sub Form2_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.TopMost = True
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Extract data ke ListBoxt
        With Form1.ListBox1
            .Items.Clear()
            Variable.Extractime.GetExtract = _Jam.Text.Replace("Jam ", "") + ":" +
                                             _Menit.Text.Replace("Menit ", "") + ":" &
                                            _tt.Text
            .Items.Add(Variable.Extractime.GetExtract)
            Form1.Timer1.Start()
            Me.Close()
        End With
    End Sub
    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Atur.NaturuLoad(_Jam, _Menit, _tt)
    End Sub
#Region "Keypress Event"
    Private Sub ComboBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles _Jam.KeyPress
        ModuleEvent.AccNumber.NumbericOnly(e)
    End Sub

    Private Sub ComboBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles _Menit.KeyPress
        ModuleEvent.AccNumber.NumbericOnly(e)
    End Sub

    Private Sub ComboBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles _tt.KeyPress
        ModuleEvent.WriteText.Disable(e)
    End Sub
#End Region
End Class