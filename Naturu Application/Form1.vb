﻿Public Class Form1
    Private tm As String
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Naturu.exe"
    End Sub
    Private Sub AturNaturuToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AturNaturuToolStripMenuItem.Click
        Form2.ShowDialog()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        MsgBox("Naturu adalah tools iseng" & vbNewLine &
               "yang dibuat hanya untuk mengisi waktu luang" & vbNewLine & vbNewLine &
               "Dev : PoetralesanA {571M Corp}" & vbNewLine & "Asal : Palu, Sulawesi Tengah" & vbNewLine & "~INDONESIA~", MsgBoxStyle.Information, "Naturu App")
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        tm = ModuleSetting.SetTime.nowtime
        Label1.Text = "Time: " & tm

        If LogicAlarm.CheckTime(tm, Variable.Extractime.GetExtract) = True Then
            Timer1.Stop()
            ShutdownDevice()
        End If
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        If MessageBox.Show("Visit Developer..?",
                           "", MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question) =
                             Windows.Forms.DialogResult.Yes Then

            Process.Start("https://www.facebook.com/poetralesana")
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Label1.Text <> "Time : Belum Aktif" Then
            If MessageBox.Show("Yakin Ingin Keluar....?" & vbNewLine &
                                "Keluar dapat menyebabkan aplikasi tidak dapat berjalan." &
                                vbNewLine & vbNewLine &
 _
 _
                                "Lanjutkan?", "WARNING!!!", MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Cancel Then
                e.Cancel = True
            Else
                Application.ExitThread()
            End If
        End If
    End Sub
End Class