﻿Namespace Atur
    Module _form2
        Private s1 As String, s2 As String
        Private j As Integer, m As Integer
        Private Sub EraseVar()
            s1 = String.Empty
            s2 = String.Empty
        End Sub
        Sub NaturuLoad(ByVal a As ComboBox,
                    ByVal b As ComboBox, ByVal c As ComboBox)
            'jam--
            s1 = "Jam"
            s2 = "Menit"
            While j <= 12
                a.Items.Add(s1 & " " & j.ToString("D2"))
                j += 1
            End While

            'menit
            While m <= 59
                b.Items.Add(s2 & " " & m.ToString("D2"))
                m += 1
            End While

            'Combobox Select
            a.SelectedIndex = 0
            b.SelectedIndex = 0
            c.SelectedIndex = 0
        End Sub

    End Module
End Namespace
